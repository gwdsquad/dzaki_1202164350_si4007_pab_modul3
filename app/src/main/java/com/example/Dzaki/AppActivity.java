package com.example.Dzaki;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AppActivity extends AppCompatActivity {
    private TextView nameInfo, jobDetails;
    private ImageView detailPict;
    private int avatarCode;
    private String dName,dJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_activity);


        nameInfo = findViewById(R.id.detail_name);
        jobDetails = findViewById(R.id.detail_job);
        detailPict = findViewById(R.id.ImgDetail);

        dName = getIntent().getStringExtra("Name");
        dJob = getIntent().getStringExtra("Job");
        avatarCode = getIntent().getIntExtra("gender",2);

        nameInfo.setText(dName);
        jobDetails.setText(dJob);
        switch (avatarCode){
            case 1 :
                detailPict.setImageResource(R.drawable.thisismale);
                break;
            case 2 :
            default:
                detailPict.setImageResource(R.drawable.thisisfemale);
                break;
        }
    }
}
