package com.example.Dzaki;
class User {

    private String Name2;
    private String Job2;
    private final int Foto1;

    public User(String Name2, String Job2, int Foto1) {
        this.Name2 = Name2;
        this.Job2 = Job2;
        this.Foto1 = Foto1;
    }

    public String getNama() {
        return Name2;
    }

    public String getPekerjaan() {
        return Job2;
    }

    public int getAvatar() {
        return Foto1;
    }
}
