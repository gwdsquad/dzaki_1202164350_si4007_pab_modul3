package com.example.Dzaki;

import android.content.Context;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
public class ForUser extends RecyclerView.Adapter<ForUser.ViewHolder> {
    private ArrayList<User> userList;
    private Context context;
    public ForUser(ArrayList<User> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public ForUser.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).
                inflate(R.layout.items, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ForUser.ViewHolder viewHolder, int urutan) {
            User currentUser = userList.get(urutan);
            viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView name, job;
        private ImageView pict;
        private int avatarCode;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.nameName);
            job = itemView.findViewById(R.id.jobJob);
            pict = itemView.findViewById(R.id.foto);

            itemView.setOnClickListener(this);
        }

        void bindTo(User currentUser){
            name.setText(currentUser.getNama());
            job.setText(currentUser.getPekerjaan());

            avatarCode = currentUser.getAvatar();
            switch (currentUser.getAvatar()){
                case 1 :
                    pict.setImageResource(R.drawable.thisismale);
                    break;
                case 2 :
                default:
                    pict.setImageResource(R.drawable.thisisfemale);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),AppActivity.class);
            toDetailActivity.putExtra("Name",name.getText().toString());
            toDetailActivity.putExtra("gender",avatarCode);
            toDetailActivity.putExtra("Job",job.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }

}
